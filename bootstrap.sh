# Bash script for vagrant provisioning, below are the essential tools for
# Drupal Development.

# Update Yum

sudo yum -y update

# Install Utility tools

sudo yum install lynx -y
sudo yum install mlocate -y
sudo yum install git -y
sudo yum install java-1.8.0-openjdk -y

# Create User and Group

username=vagrant
groupname=smbgrp
sudo groupadd $groupname
sudo usermod -g $groupname $username

# Apache

sudo yum install httpd -y
sudo /sbin/service httpd start
sudo systemctl enable httpd

# MySql Enterprise | Oracle

sudo rpm -Uvh http://dev.mysql.com/get/mysql-community-release-el7-5.noarch.rpm
sudo yum -y install mysql-community-server
sudo /usr/bin/systemctl enable mysqld
sudo /usr/bin/systemctl start mysqld

# PHP 5.6

sudo yum -y remove php*
sudo yum -y update

sudo wget http://dl.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-5.noarch.rpm
sudo wget http://rpms.famillecollet.com/enterprise/remi-release-7.rpm
sudo rpm -Uvh remi-release-7*.rpm epel-release-7*.rpm

FILE="/etc/yum.repos.d/remi.repo"
sudo touch $FILE
sudo chmod 0777 $FILE

sudo echo "# Repository: http://rpms.remirepo.net/
# Blog:       http://blog.remirepo.net/
# Forum:      http://forum.remirepo.net/

[remi]
name=Remi's RPM repository for Enterprise Linux 7 - $basearch
#baseurl=http://rpms.remirepo.net/enterprise/7/remi/$basearch/
mirrorlist=http://rpms.remirepo.net/enterprise/7/remi/mirror
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-remi

[remi-php56]
name=Remi's PHP 5.6 RPM repository for Enterprise Linux 7 - $basearch
#baseurl=http://rpms.remirepo.net/enterprise/7/php56/$basearch/
mirrorlist=http://rpms.remirepo.net/enterprise/7/php56/mirror
# WARNING: If you enable this repository, you must also enable "remi"
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-remi" >> $FILE

sudo yum -y install php php-cli php-devel php-dba php-pdo php-xml php-pecl-gearman php-intl php-mbstring php-pecl-geoip php-pecl-igbinary-devel php-xmlrpc php-common php-gd php-embedded php-pecl-apcu-devel php-interbase php-opcache php-pecl-igbinary php-pecl-imagick-devel php-snmp php-pecl-memcache php-bcmath php-pecl-apcu php-mcrypt php-ldap php-pecl-imagick php-pecl-memcached php-soap php-enchant php-fpm php-process php-pear php-imap php-mysql php-tidy php-pecl-redis php-pecl-xdebug

# Composer

curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

# Drush

composer global require drush/drush:7.*
sudo ln -s /home/vagrant/.composer/vendor/bin/drush /usr/local/bin/


# MongoDB

FILE1="/etc/yum.repos.d/mongodb.repo"
sudo touch $FILE1
sudo chmod -R 0777 $FILE1

sudo echo "[mongodb]
name=MongoDB Repository
baseurl=http://downloads-distro.mongodb.org/repo/redhat/os/x86_64/
gpgcheck=0
enabled=1" >> $FILE1

sudo yum -y update
sudo yum -y install mongodb-org mongodb-org-server
sudo systemctl start mongod
sudo systemctl status mongod

# PHPUnit

sudo wget https://phar.phpunit.de/phpunit.phar --no-check-certificate
sudo chmod +x phpunit.phar
sudo mv phpunit.phar /usr/local/bin/phpunit

# Jenkins

sudo wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo
sudo rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key
sudo yum install jenkins -y
sudo service jenkins start
sudo systemctl enable jenkins.service
sudo systemctl status jenkins.service

# Samba

sudo yum -y install samba samba-client samba-common
FILE2="/etc/samba/smb.conf"
sudo mv $FILE2 /etc/samba/smb.conf.bak
sudo touch $FILE2
sudo chmod -R 0777 $FILE2

echo "[global]
workgroup = CORP
server string = Samba Server %v
netbios name = centos7
security = user
map to guest = bad user
dns proxy = no
passdb backend = tdbsam
guest account = vagrant
log file = /var/log/samba/log.%m
max log size = 50

#=========== Share Definitions ===========
[Vagrant]
path = /var/www/html
browsable =yes
writable = yes
guest ok = yes
read only = no" >> $FILE2

sudo chmod -R 0777 /var/www/html/
sudo chcon -t public_content_rw_t /var/www/html/
sudo setsebool -P allow_smbd_anon_write 1
sudo setsebool -P allow_httpd_anon_write 1
sudo systemctl enable smb.service
sudo systemctl enable nmb.service
sudo systemctl restart smb.service
sudo systemctl restart nmb.service

# Elasticsearch

sudo rpm --import https://packages.elastic.co/GPG-KEY-elasticsearch
sudo touch /etc/yum.repos.d/elasticsearch.repo
FILE3="/etc/yum.repos.d/elasticsearch.repo"
echo "[elasticsearch-1.7]
name=Elasticsearch repository for 1.7.x packages
baseurl=http://packages.elastic.co/elasticsearch/1.7/centos
gpgcheck=1
gpgkey=http://packages.elastic.co/GPG-KEY-elasticsearch
enabled=1" >> $FILE3
sudo chmod -R 0777 /etc/yum.repos.d/elasticsearch.repo
sudo chown -R $username:$groupname /etc/yum.repos.d/elasticsearch.repo
sudo yum -y install elasticsearch
sudo systemctl daemon-reload
sudo systemctl enable elasticsearch.service
sudo systemctl start elasticsearch.service

# Graylog 2

sudo rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

# Stop Routings

sudo iptables -F
sudo systemctl stop firewalld
sudo systemctl disable firewalld

# Update and restart

sudo /sbin/service httpd restart
sudo /sbin/service network restart
sudo updatedb

# Adding commands to check service status

systemctl status httpd
systemctl status mysqld
php --version
systemctl status smb.service
systemctl status mongod
sudo systemctl status elasticsearch.service
systemctl status jenkins
